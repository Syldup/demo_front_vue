import Vue from 'vue'
import VueRouter from 'vue-router'
import ApiService from '@/services/api.service'

Vue.use(VueRouter)

const router = new VueRouter({
   routes: [
      {path: '/', name: 'Home', component: () => import('./views/Home')},
      {path: '/login', name: 'Login', component: () => import('./views/Login')},
      {path: '*', name: 'Other', component: () => import('./views/Home')},
   ]
})


router.beforeEach((to, from, next) => {
   // redirect to login page if not logged in and trying to access a restricted page
   const publicPages = ['/login', '/'];
   const authRequired = !publicPages.includes(to.path);

   if (authRequired && !ApiService.hasToken) {
      return next('/login');
   }

   next();
})

export default router;
