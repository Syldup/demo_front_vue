import Vue from 'vue'
import axios from 'axios'
import VueAxios from 'vue-axios'
import {API_URL} from './config'

Vue.use(VueAxios, axios)
Vue.axios.defaults.baseURL = API_URL

// function parseJwt(token) {
//   var base64Url = token.split('.')[1];
//   var base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
//   var jsonPayload = decodeURIComponent(atob(base64).split('').map(function (c) {
//     return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
//   }).join(''));
//
//   return JSON.parse(jsonPayload);
// }

const ApiService = {
  hasToken: false,
  get(resource, onNext) {
    if (!onNext)
      return Vue.axios
        .get(resource)
        .catch(error => {
          throw new Error(`ApiService ${error}`)
        })
    return new Promise((resolve) => {
      function getFromUrl(url) {
        Vue.axios
          .get(url)
          .catch(error => {
            throw new Error(`ApiService ${error}`)
          })
          .then(({data}) => {
            if (onNext(data.results) && data.next)
              getFromUrl(data.next)
            else resolve()
          })
      }

      getFromUrl(resource)
    })
  },
  post(resource, body) {
    return Vue.axios
      .post(resource, body)
      .catch(error => {
        throw new Error(`ApiService ${error}`)
      })
  },
  put(resource, body) {
    return Vue.axios
      .put(resource, body)
      .catch(error => {
        throw new Error(`ApiService ${error}`)
      })
  },
  setToken(token) {
    if (token && 'access' in token) {
      // console.log(parseJwt(token.refresh))
      // console.log(parseJwt(token.access))
      localStorage.setItem('token', JSON.stringify(token));
      Vue.axios.defaults.headers = {
        Authorization: `Bearer ${token.access}`
      };
      this.hasToken = true;
    }
  },
  clearToken() {
    this.hasToken = false;
    Vue.axios.defaults.headers = {}
    // remove user from local storage to log user out
    localStorage.removeItem('token');
  }
}
export default ApiService
