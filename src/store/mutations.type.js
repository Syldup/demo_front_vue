
// global
export const FETCH_START = "loadingOn"
export const FETCH_END = "loadingOff"

export const SET_NUMS = "setNums"
export const SET_TOKEN = "setToken"
export const SET_SUM = "setSum"
