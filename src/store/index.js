import Vue from 'vue'
import Vuex from 'vuex'
import {FETCH_END, FETCH_START, SET_NUMS, SET_SUM, SET_TOKEN} from "@/store/mutations.type";
import {FETCH_NUMS, LOGIN, LOGOUT, POST_NUMS, ADD_NUM, SUM_NUMS, CLEAR_NUMS} from "@/store/actions.type";
import ApiService from "@/services/api.service";

Vue.use(Vuex)

const NUM_KEY = 'NUM'
const AUTH_KEY = 'AUTH'
const localToken = JSON.parse(localStorage.getItem('token'));
ApiService.setToken(localToken);

export default new Vuex.Store({
  state: {
    token: localToken,
    loading: {},
    nums: [],
    sum: {},
  },
  getters: {
    auth(state) {
      return state.token !== null
    },
    nums(state) {
      return state.nums
    },
    sum(state) {
      return state.sum
    },
    numsIsLoading(state) {
      return state.loading[NUM_KEY] || false;
    },
    authIsLoading(state) {
      return state.loading[AUTH_KEY] || false;
    },
  },
  actions: {
    [LOGIN](context, payload) {
      context.commit(FETCH_START, AUTH_KEY)

      const {username, password} = payload;
      return ApiService
        .post(`token/`, {username, password})
        .then(({data}) => {
          console.log('api/token/ =>', data);

          ApiService.setToken(data);
          context.commit(SET_TOKEN, data);
          context.commit(FETCH_END, AUTH_KEY)
        }).catch(err => {
          console.log(err);
          context.commit(FETCH_END, AUTH_KEY)
          context.commit(SET_TOKEN, null);
        });
    },
    [LOGOUT](context) {
      ApiService.clearToken();
      context.commit(SET_TOKEN, null);
    },
    [FETCH_NUMS](context) {
      context.commit(FETCH_START, NUM_KEY)

      return ApiService
        .get(`table/`)
        .then(({data}) => {
          context.commit(SET_NUMS, data);
          context.commit(FETCH_END, NUM_KEY)
        }).catch(err => {
          console.log(err);
          context.commit(FETCH_END, AUTH_KEY)
          context.commit(SET_TOKEN, null);
        });
    },
    [POST_NUMS](context) {
      context.commit(FETCH_START, NUM_KEY)

      context.state.nums
        .filter(obj => 'id' in obj && Object.entries(obj.last).some(([key, val]) => obj[key] !== val))
        .forEach(obj => {
          ApiService
            .put(`table/${obj.id}/`, {num: obj.num, num_back: obj.num_back})
            .then(console.log)
            .catch(console.log);
        })

      const toCreate = context.state.nums.filter(val => !('id' in val))
      return ApiService
        .post(`table/`, toCreate)
        .then((data) => {
          console.log(data);
          context.commit(FETCH_END, NUM_KEY)
        }).catch(err => {
          console.log(err);
          context.commit(FETCH_END, NUM_KEY)
        });
    },
    [ADD_NUM](context, {val, back}) {
      if (!back)
        return context.commit(ADD_NUM, val)

      return ApiService
        .post(`table/`, {num: val, num_back: val})
        .then(console.log).
        catch(console.log);
    },
    [SUM_NUMS](context, back) {
      const request = back ? ApiService.get(`sum/`) :
        ApiService.post(`sum/`, context.state.nums.map(obj => {
          return {num: obj.num, num_back: obj.num_back}
        }))

      return request
        .then(({data}) => {
          console.log(data);
          context.commit(SET_SUM, data)
        }).catch(console.log);
    },
    [CLEAR_NUMS](context, back) {
      if (!back)
        return context.commit(SET_NUMS, [])

      return ApiService.post(`clear/`)
        .then(console.log)
        .catch(console.log);
    },
  },
  mutations: {
    [FETCH_START](state, payload) {
      Vue.set(state.loading, payload, true)
      // state.loading[payload] = true
    },
    [FETCH_END](state, payload) {
      Vue.set(state.loading, payload, false)
      // state.loading[payload] = false
    },
    [SET_NUMS](state, payload) {
      state.nums = payload.map(val => {
        val.last = {...val};
        return val;
      })
    },
    [SET_TOKEN](state, token) {
      state.token = token;
    },
    [ADD_NUM](state, payload) {
      state.nums.push({num: payload, num_back: payload})
    },
    [SET_SUM](state, payload) {
      state.sum = payload
    },
  },
})
