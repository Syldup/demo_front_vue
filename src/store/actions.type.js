
// related to AUTH
export const LOGIN = "login"
export const LOGOUT = "logout"

export const FETCH_NUMS = "fetchNums"
export const POST_NUMS = "postNums"
export const ADD_NUM = "addNum"
export const SUM_NUMS = "sumNums"
export const CLEAR_NUMS = "clearNums"
