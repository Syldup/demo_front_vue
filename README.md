# Installer le Front vue.js (en ligne de commande)

## Installer Node js
Télécharger la dernière version de Node.js (12.16.3)
[Node-download](https://nodejs.org/en/download/)  
Lancer l'executable, Laisser les options par défaut.  
Node est bien installer si vous pouvez faire la commande suivant dans un terminal
```
npm
```
Si la commande n'existe pas redémarrer le PC (J'ai eu ce problème)

## Installer le projet
Ouvrez un terminal, se rendre dans le repertoire où installer le projet  
Normalement, vous avez git sinon installer le.
[Git-download](https://git-scm.com/downloads)  
Télécharger, puis installer le projet avec les commandes suivantes :
```
git clone https://gitlab.com/Syldup/demo_front_vue.git
cd demo_front_vue
npm install
```
Attendre que ça s'installe

### Démarrer le server pour development
```
npm run serve
```
Attendre que ça compile

Le site est accessible à cette adresse http://localhost:8080/  
Il y a deux pages : un Home et une pour se connecter

Des infos complémentaires sont sur le site en question.  
(Pour acceder au log et erreur de la page faure F12 sur votre navigateur, dans l'onglet console)

### Compiler pour la production (ne pas faire)
```
npm run build
```

### Récupérer les dernières modification (si il y en a)
```
git pull
```
